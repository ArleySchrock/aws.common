﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AWS.Common.Events
{
    public class Events
    {
        public class EventArgs<T> : System.EventArgs
        {
            public EventArgs()
            {
                Value = default(T);
                Success = true;
                Continue = true;
            }

            public EventArgs(T value, bool success = true, bool @continue = true)
                : this()
            {
                this.Value = value;
                this.Success = success;
                this.Continue = @continue;
            }

            public bool Continue { get; set; }

            public bool Success { get; set; }

            public T Value { get; set; }
        }

        public static EventArgs<RT> Get<RT>()
        {
            return Get(default(RT));
        }

        public static EventArgs<RT> Get<RT>(RT value)
        {
            return Get(value, true, true);
        }

        public static EventArgs<RT> Get<RT>(RT value, bool success, bool @continue = true)
        {
            return new EventArgs<RT>(value, @success, @continue);
        }

        public delegate void DynamicEventHandler(object sender, EventArgs<dynamic> e);

        public delegate void EventHandler<T>(object sender, EventArgs<T> e);
    }
}
