﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Linq;

using Newtonsoft.Json;

namespace AWS.Common
{
    public static class IEnumerableExtensions
    {
        public static IEnumerable<R> As<T, R>(this IEnumerable<T> coll, Func<T, R> converter)
        {
            return coll.Each(converter);
        }

        public static IEnumerable<R> As<T, R>(this IEnumerable<T> coll)
            where R : new()
        {
            Func<T, R> convert = (from) =>
                {
                    return from.ToDictionary().Into<R>(new R());
                };
            return coll.Each(convert);
        }

        public static void Each<T>(this IEnumerable<T> coll, Func<T, bool> eval, Action<T> action)
        {
            foreach (var target in coll.Where(x => eval(x)))
            {
                action(target);
            }
        }
        public static IEnumerable<R> Each<T, R>(this IEnumerable<T> coll, Func<T, bool> eval, Func<T, R> action)
        {
            List<R> results = new List<R>();
            foreach (var target in coll.Where(x => eval(x)))
            {
                results.Add(action(target));
            }
            return results;
        }

        public static IEnumerable<R> Each<T, R>(this IEnumerable<T> coll, Func<T, R> action)
        {
            return coll.Each(x => true, action);
        }

        public static void Each<T>(this IEnumerable<T> coll, Action<T> action)
        {
            coll.Each(x => true, action);
        }
    }

    public static class JsonConversions
    {
        public static string ToJson(this object obj)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(obj);
        }

        public static string ToJson(this XmlNode node)
        {
            return JsonConvert.SerializeXmlNode(node);
        }

        public static string ToJson(this XNode xnode)
        {
            return JsonConvert.SerializeXNode(xnode);
        }
    }

    public static class ObjectConversions
    {
        public static T Into<T>(this IDictionary<string, dynamic> dict, T template)
        {
            var type = typeof(T);
            foreach (var item in dict)
            {
                try
                {
                    type.GetProperty(item.Key).SetValue(template, item.Value, null);
                }
                catch
                {
                    try
                    {
                        type.GetField(item.Key).SetValue(template, item.Value);
                    }
                    catch { }
                }
            }
            return template;
        }

        public static T To<T>(this IDictionary<string, dynamic> dictionary) where T : new()
        {
            T t = new T();
            var type = typeof(T);
            foreach (var item in dictionary)
            {
                try
                {
                    type.GetProperty(item.Key).SetValue(t, item.Value, null);
                }
                catch
                {
                    try
                    {
                        type.GetField(item.Key).SetValue(t, item.Value);
                    }
                    catch { }
                }
            }
            return t;
        }

        public static IDictionary<string, dynamic> ToDictionary(this object obj)
        {
            Dictionary<string, dynamic> result = new Dictionary<string, dynamic>();
            foreach (dynamic item in obj.GetType().GetProperties().
                Select(x => (dynamic)x).Union(obj.GetType().GetFields()))
            {
                result[item.Name] = item.GetValue(obj);
            }
            return result;
        }
    }

    public static class StringConversions
    {
        public static T FromJson<T>(this string json)
        {
            return JsonConvert.DeserializeObject<T>(json);
        }
        public static T FromJson<T>(this string json, T template)
        {
            JsonConvert.PopulateObject(json, template);
            return template;
        }
    }
}
